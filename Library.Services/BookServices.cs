﻿using System;
using System.Web;
using System.Collections.Generic;
using NLog;
using Library.Core.Entities;
using Library.Data.Interfaces.Services;
using Library.Data.Interfaces.Base;
using Library.Data.Repositories;
using Library.Data;

namespace Library.Services
{
    public class BookServices
    {
        private readonly BookRepository booksRepository;

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public BookServices(string connectionString)
        {
            var context = new DataContext(connectionString);

            booksRepository = new BookRepository(context);
        }

        public Book CreateBook(ICollection<Author> authors, string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("The name parameter in the CreateBook method cannot be null or empty!", "name");
            }

            else if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("The name parameter in the CreateBook method cannot be null or white space!", "name");
            }

            return new Book { Authors = authors, Name = name };
        }

        public Book CreateBook(Book book)
        {
            if (book == null)
            {
                logger.Error("Failed to create a new book because the book parameter is null.");
                return null;
            }

            return new Book() { Authors = book.Authors, Name = book.Name };
        }

        public Book AddBook(Book book)
        {
            try
            {
                return booksRepository.Add(book);
            }

            catch (ArgumentNullException exception)
            {
                logger.Error(exception, "Error in AddBook method.");
                return null;
            }
        }

        public Book UpdateBook(long id, Book newBook)
        {
            Book book = GetBookById(id);

            if (book == null)
            {
                return null;
            }
            book.Authors = newBook.Authors;

            book.CountOfPages = newBook.CountOfPages;

            book.Genre = newBook.Genre;

            book.Name = newBook.Name;

            book.Price = newBook.Price;

            book.Title = newBook.Title;

            book.Year = newBook.Year;
           

            if (!booksRepository.Edit(book))
            {
                logger.Error("Error in UpdateBook method.");

                return null;
            }

            booksRepository.SaveChanges();

            return book;

        }

        public bool DeleteBook(long id)
        {
            Book book = GetBookById(id);

            if (book != null)
            {
                if (!booksRepository.Delete(book))
                {
                    logger.Error("Error in Delete method.");
                    return false;
                }

                return true;
            }

            return false;
        }

        public Book GetBookById(long id)
        {
            Book book = booksRepository.GetById(id);

            if (book == null)
            {
                logger.Error("Error in GetBookById method.");
            }

            return book;
        }

        public Book GetBookByName(string name)
        {
            Book book = booksRepository.GetByName(name);

            if (book == null)
            {
                logger.Error("Error in GetBookByName method.");
            }

            return book;
        }

        public IEnumerable<Book> GetAllBooks()
        {
            IEnumerable<Book> books = booksRepository.GetAll();

            if (books == null)
            {
                logger.Error("Error in GetAllBooks method.");
            }

            return books;
        }

        public IEnumerable<Author> GetAuthorsByBook(Book book)
        {
            return book.Authors;
        }

        public IEnumerable<Book> GetBooksWithAuthors()
        {
            IEnumerable<Book> books = booksRepository.GetAll();

            return books;

        }
        public bool SaveBooks()
        {
            try
            {
                booksRepository.SaveChanges();
            }

            catch (Exception exception)
            {
                logger.Error(exception, "Error in SaveBooks method.");
                throw;
            }

            return true;

        }

    }
}
