﻿using System.Collections.Generic;

namespace Library.Core.Entities
{
    public class Author : BaseEntity
    {

        #region Relationships

        public virtual ICollection<Book> Books { get; set; }

        #endregion

    }
}
