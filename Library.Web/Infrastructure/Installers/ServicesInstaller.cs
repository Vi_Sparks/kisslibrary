﻿using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Library.Data.Interfaces.Services;
using Library.Services;

namespace Library.Web.Infrastructure.Installers
{
    public class ServicesInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For(typeof(IBookServices)).ImplementedBy(typeof(BookServices)).LifeStyle.PerWebRequest,
                Component.For(typeof(IAuthorServices)).ImplementedBy<AuthorServices>().LifeStyle.PerWebRequest);

        }
    }
}