﻿using System;
using System.Web.Mvc;
using System.Linq;
using System.Web;
using System.Net.Http;
using Library.Core.Entities;
using Library.Data.Interfaces.Services;
using Library.View.BookViewModel;
using Library.Services;
using System.Configuration;

namespace Library.Web.Controllers
{
    public class BookController : Controller
    {
        protected readonly BookServices bookServices;

        public BookController()
        {
            this.bookServices = new BookServices(ConfigurationManager.ConnectionStrings["LibraryDatabase"].ToString());
        }

        public ActionResult Index()
        {
            return View("Index");
        }

        [HttpPost]
        public ActionResult CreateBook()
        {
            Book book = new Book();

            return PartialView("CreateBook");
        }


        [HttpPost]
        public JsonResult UpdateBook(BookUpdateBookViewModel view)
        {
            try
            {
                Book book = this.bookServices.GetBookById(view.Id);

                if (book != null)
                {
                    bookServices.UpdateBook(view.Id, book);

                    return Json(view);
                }
                return Json($"The book with id {view.Id} was not found in the database", JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                
                return Json($"There was an error updating book {view.Id}", JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public void DeleteBook(long id)
        {
            this.bookServices.DeleteBook(id);

            this.bookServices.SaveBooks();
        }

        public JsonResult ListOfBooks()
        {
            var jsondata = (from book in bookServices.GetBooksWithAuthors()
                            select new BookUpdateBookViewModel(book));

            return Json(jsondata, JsonRequestBehavior.AllowGet);
        }

    }
}