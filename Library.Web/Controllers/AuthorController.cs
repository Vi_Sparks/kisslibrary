﻿using System;
using System.Web.Mvc;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Collections.Generic;
using Library.Core.Entities;
using Library.Data.Interfaces.Services;
using Library.Services;
using System.Configuration;

namespace Library.Web.Controllers
{
    public class AuthorController : Controller
    {
        protected readonly BookServices bookServices;

        protected readonly AuthorServices authorServices;

        HttpRequest request = System.Web.HttpContext.Current.Request;

        public ActionResult Index()
        {
            return View();
        }

        public AuthorController()
        {
            this.bookServices = new BookServices(ConfigurationManager.ConnectionStrings["LibraryDatabase"].ToString());
        }

        public JsonResult ListOfAuthorsByBook(long id)
        {

            IEnumerable<Author> authors = bookServices.GetAuthorsByBook(bookServices.GetBookById(id));

            var jsondata = (from o in authors
                            select new
                            {
                                Id = o.Id,
                                Name = o.Name,
                                Books = string.Join(",", o.Books.Select(book => book.Title.ToString()))
                            });

            return Json(jsondata, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public HttpResponseMessage UpdateAuthor(long id)
        {

            var response = new HttpResponseMessage();

            Author author = this.authorServices.GetAuthorById(id);

            try
            {

                if (author != null)
                {
                    author.Name = string.IsNullOrEmpty(request["Name"]) ? author.Name : Request["Name:"];
                    authorServices.UpdateAuthor(id, author);
                    authorServices.SaveAuthors();
                }

                else
                {
                    response.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                    response.Content = new StringContent(string.Format("The author with id {0} was not found in the database"));
                }
            }
            catch (Exception)
            {
                response.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.Content = new StringContent(string.Format("There was an error updating author {0}"));
            }

            return response;
        }
    }
}