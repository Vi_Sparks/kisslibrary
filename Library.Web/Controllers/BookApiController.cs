﻿using Library.Core.Entities;
using Library.Services;
using Library.View.BookViewModel;
using System;
using System.Configuration;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace Library.Web.Controllers
{
    [RoutePrefix("api/books")]
    public class BookApiController : ApiController
    {
        protected readonly BookServices bookServices;

        public BookApiController()
        {
            this.bookServices = new BookServices(ConfigurationManager.ConnectionStrings["LibraryDatabase"].ToString());
        }

        [Route("update")]
        [HttpPost]
        public IHttpActionResult Update([ModelBinder(typeof(BookModelBinderProvider))]BookUpdateBookViewModel view)
        {
            //var view = Request.Content.ReadAsStringAsync();
            //var jsonrequest = Json(view);

            if (view == null)
            {
                return BadRequest();
            }
            try
            {
                Book book = this.bookServices.GetBookById(view.Id);

                if (book == null)
                {
                    return BadRequest($"The book with id {view.Id} was not found in the database");
                }

                bookServices.UpdateBook(view.Id, book);

                bookServices.SaveBooks();

                return Ok(view);
            }
            catch (Exception)
            {
                return BadRequest($"There was an error updating book {view.Id}");
            }
        }
    }
}
