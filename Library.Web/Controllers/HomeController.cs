﻿using System.Web.Mvc;
using Library.Data.Interfaces.Services;

namespace Library.Web.Controllers
{
    public class HomeController : Controller
    {

        public HomeController()
        {

        }

        public ActionResult Index()
        {
            return View("Index");
        }
    }
}