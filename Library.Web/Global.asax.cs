﻿using System.Web.Mvc;
using System.Web.Routing;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Library.Web.Infrastructure.Resolvers;
using System.Data.Entity;
using Library.Data;
using System.Web.Http;

namespace Library.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static IWindsorContainer container;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DataContext>());
            InitializeContainer();

        }

        protected void Application_End()
        {
            container.Dispose();
        }

        private static void InitializeContainer()
        {
            container = new WindsorContainer().Install(FromAssembly.This());
            DependencyResolver.SetResolver(new WindsorDependencyResolver(container));
        }
    }
}
