﻿using System;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace Library.Web
{
    public class BookModelBinderProvider : ModelBinderProvider
    {
            public override IModelBinder GetBinder(HttpConfiguration configuration, Type modelType)
            {
                return new BookModelBinder();
            }
    }
}