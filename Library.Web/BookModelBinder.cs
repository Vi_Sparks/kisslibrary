﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using System.Web.Http.ValueProviders;
using System.Net;
using Library.Core.Entities;
using Library.View.BookViewModel;

namespace Library.Web
{
    public class BookModelBinder : IModelBinder
    {
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            BookUpdateBookViewModel book = new BookUpdateBookViewModel();

            if (bindingContext.ModelType != typeof(BookUpdateBookViewModel))
            {
                return false;
            }

            string requestResult = actionContext.Request.Content.ReadAsStringAsync().Result;

            string[] values = requestResult.Split(',');

            var fields = from value in values select value.Substring(value.LastIndexOf(':') + 1).Trim('}');

            book.Id = Convert.ToInt64(fields.ElementAt(0));

            book.Name = fields.ElementAt(1) == "null" ? null : fields.ElementAt(1);

            book.CountOfPages = Convert.ToInt32(fields.ElementAt(2));

            book.Title = fields.ElementAt(3).Trim('"') == "null" ? null : fields.ElementAt(3).Trim('"');

            book.Year = Convert.ToInt32(fields.ElementAt(4));

            book.Price = Convert.ToSingle(fields.ElementAt(5));

            book.Genre = fields.ElementAt(6).Trim('"') == "null" ? null : fields.ElementAt(6).Trim('"');

            book.Content = fields.ElementAt(7) == "null" ? null : fields.ElementAt(7);

            book.Authors = fields.ElementAt(8).Trim('[', ']', '"').Split(',');

            bindingContext.Model = book;

            return true;

        }
    }
}