﻿using System.Data.Entity.ModelConfiguration;
using Library.Core.Entities;

namespace Library.Data.Configurations
{
    public class BookConfiguration : EntityTypeConfiguration<Book>
    {
        public BookConfiguration()
        {
            HasKey(book => book.Id);
            HasMany(book => book.Authors).WithMany(author => author.Books);
        }
    }
}

