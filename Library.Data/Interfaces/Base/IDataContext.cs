﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace Library.Data.Interfaces.Base
{
    public interface IDataContext
    {
        Database Database { get; }

        DbContextConfiguration Configuration { get; }

        DbChangeTracker ChangeTracker { get; }

        int SaveChanges();

        DbEntityEntry<T> Entry<T>(T entity) where T : class;

        DbSet<T> Set<T>() where T : class;
    }
}

