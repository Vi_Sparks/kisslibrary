﻿using System.Linq;
using Library.Core.Entities;

namespace Library.Data.Interfaces.Base
{
    public interface IRepository<T> where T : BaseEntity
    {
        IQueryable<T> GetAll();
        
        T Add(T entity);

        bool Delete(T entity);

        bool Edit(T entity);

        bool SaveChanges();

        T GetById(long id);

        T GetByName(string name);
    }
}

