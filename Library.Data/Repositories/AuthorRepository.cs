﻿using Library.Core.Entities;

namespace Library.Data.Repositories
{
    public class AuthorRepository : Repository<Author>
    {
        private DataContext _dataContext;

        public AuthorRepository(DataContext context) : base(context)
        {
            _dataContext = context;
        }
    }
}
