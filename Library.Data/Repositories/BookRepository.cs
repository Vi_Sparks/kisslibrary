﻿using Library.Core.Entities;
using System.Collections.Generic;

namespace Library.Data.Repositories
{
    public class BookRepository : Repository<Book>
    {
        private DataContext _dataContext;

        public BookRepository(DataContext context) : base(context)
        {
            _dataContext = context;
        }
    }
}
