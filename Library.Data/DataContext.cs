﻿using System.Data.Entity;
using Library.Data.Interfaces.Base;
using Library.Data.Configurations;
using Library.Core.Entities;

namespace Library.Data
{
    public class DataContext : DbContext, IDataContext
    {
        public DbSet<Author> Authors { get; set; }

        public DbSet<Book> Books { get; set; }

        public DataContext()
        {

        }

        public DataContext(string connectionString) : base(connectionString)
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new BookConfiguration());

            modelBuilder.Configurations.Add(new AuthorConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}

