﻿using Library.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.View.BookViewModel
{
    public class BookUpdateBookViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public int CountOfPages { get; set; }

        public string Title { get; set; }

        public int Year { get; set; }

        public float Price { get; set; }

        public string Genre { get; set; }

        public string Content { get; set; }

        public IEnumerable<string> Authors { get; set; }

        public BookUpdateBookViewModel()
        {

        }

        public BookUpdateBookViewModel(Book book)
        {
            this.Id = book.Id;

            this.Title = book.Title;

            this.Year = book.Year;

            this.Price = book.Price;

            this.Genre = book.Genre;

            this.Content = book.Content;

            this.Authors = (book.Authors).Select(author => author.Name);
        }
    }
}
